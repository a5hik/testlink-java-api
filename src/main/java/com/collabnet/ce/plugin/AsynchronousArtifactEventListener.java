package com.collabnet.ce.plugin;

import com.collabnet.ce.soap60.types.SoapFieldValues;
import com.collabnet.ce.soap60.webservices.ClientSoapStubFactory;
import com.collabnet.ce.soap60.webservices.cemain.ICollabNetSoap;
import com.collabnet.ce.soap60.webservices.cemain.TrackerFieldSoapDO;
import com.collabnet.ce.soap60.webservices.tracker.ArtifactSoapDO;
import com.collabnet.ce.soap60.webservices.tracker.ITrackerAppSoap;
import com.testlink.api.TestLinkAPIClient;
import com.testlink.api.common.TestLinkAPIException;
import com.testlink.api.domain.TestProject;
import com.testlink.api.domain.TestSuite;
import com.vasoftware.sf.events.EventHandler60;
import org.apache.axis.AxisFault;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.xml.namespace.QName;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Properties;

public class AsynchronousArtifactEventListener extends EventHandler60 {

    private static final String WEB_SERVICE_ENDPOINT_PROPERTY = "webServiceEndpoint";
    private static final String TEST_LINK_ENDPOINT_PROPERTY = "testLinkEndpoint";
    private static final String TEST_LINK_API_KEY_PROPERTY = "testLinkApiKey";
    private static final String CTF_TEST_LINK_FIELD_NAME = "ctffieldName";
    private static final String CTF_TEST_LINK_FIELD_VALUE = "ctffieldValue";

    private static final String PROPERTIES_FILE = "META-INF/config.properties";

    public final static Log log = LogFactory.getLog(AsynchronousArtifactEventListener.class);

    private boolean initialized = false;
    private String defaultWebserviceEndpoint = "http://localhost:8080";
    private String testLinkServiceEndpoint;
    private String testLinkApiKey;
    private String ctfFieldName;
    private String ctfFieldValue;


    public  AsynchronousArtifactEventListener() {
        initialize();
    }

    private synchronized void initialize() {
        if (initialized ) {
            return;
        }
        initialized = true;

        try {
            InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILE);
            Properties prop = new Properties();
            prop.load(inputStream);
            String webServiceEndpoint = prop.getProperty(WEB_SERVICE_ENDPOINT_PROPERTY);
            if (webServiceEndpoint != null) {
                defaultWebserviceEndpoint = webServiceEndpoint;
            }

            testLinkServiceEndpoint = prop.getProperty(TEST_LINK_ENDPOINT_PROPERTY);
            testLinkApiKey = prop.getProperty(TEST_LINK_API_KEY_PROPERTY);
            ctfFieldName = prop.getProperty(CTF_TEST_LINK_FIELD_NAME);
            ctfFieldValue = prop.getProperty(CTF_TEST_LINK_FIELD_VALUE);

        } catch (Exception e) {
            log.error("While reading the properties file "+ PROPERTIES_FILE +
                    " an error occured", e);
        }
    }

    @Override
    public void processEvent() throws Exception {

        ITrackerAppSoap trackerClient = (ITrackerAppSoap) ClientSoapStubFactory.getSoapStub(ITrackerAppSoap.class, defaultWebserviceEndpoint);
        ArtifactSoapDO artifact = (ArtifactSoapDO) getUpdatedData();
        TrackerFieldSoapDO[] fields = trackerClient.getFields(getSessionKey(), artifact.getFolderId());
        boolean processEvent = false;

        SoapFieldValues flexFields = artifact.getFlexFields();

        for(TrackerFieldSoapDO field: fields) {
            System.out.println(field.getName() + field.getDisabled());
            if(field.getName().equals(ctfFieldName) && Arrays.asList(flexFields.getNames()).contains(field.getName())
                    && !field.getDisabled()) {
                if(field.getFieldValues() != null && field.getFieldValues().length > 0
                    && field.getFieldValues()[0].getValue() != null
                    && field.getFieldValues()[0].getValue().equals(ctfFieldValue)) {
                    processEvent = true;
                    break;
                }
            }
        }
        System.out.println(processEvent);
        if(!processEvent) return;

        String originId = artifact.getId();
        //get the target id.

        String targetId = getTargetId(originId);
        String operation = getEventContext().getOperation();

        //targetId is not null only when the testsuite is created first time,
        //can happen in create, else in update.
        System.out.println("targetId: " + targetId);
        System.out.println("operation: " + operation);

        if (targetId != null && (operation.equals("create")
                || operation.equals("update"))) {
            ICollabNetSoap sfApp = (ICollabNetSoap) ClientSoapStubFactory.getSoapStub(ICollabNetSoap.class, defaultWebserviceEndpoint);
            boolean finishedUpdate = false;
            int numberOfTries = 0;
            while (!finishedUpdate && numberOfTries++ < 10) {
                try {
                    String description = "Create SOAP Association for TestSuite Creation.";
                    sfApp.createAssociation(getSessionKey(), originId, targetId, description);
                    finishedUpdate = true;
                } catch (AxisFault e) {
                    QName faultCode = e.getFaultCode();
                    if (!faultCode.getLocalPart().equals("VersionMismatchFault")) {
                        throw e;
                    }
                }
            }
        }
    }

   private String getTargetId(String artifactId) {
        String suiteId = null;
        try {
            TestLinkAPIClient client = new TestLinkAPIClient(testLinkServiceEndpoint, testLinkApiKey);
            TestProject project = client.getTestProjectByName("ctftest");
            TestSuite suite = client.createTestSuite(project.getId(), artifactId, "created from API");
            suiteId = suite.getId().toString();
        } catch (TestLinkAPIException e) {
            System.out.println(e.getMessage());
            log.info(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            log.error(e.getMessage());
        }
        return suiteId;
    }
}