package com.testlink.api.common;


import java.util.HashMap;
import java.util.Map;

public final class TestLinkHelper {

    public static final Object[] EMPTY_ARRAY = new Object[0];
    public static final Map<String, Object> EMPTY_MAP = new HashMap<String, Object>();

    /**
     * @param map
     * @param key
     * @return String value.
     */
    public static String getString(Map<String, Object> map, String key) {
        String string = null;
        if (map != null && map.size() > 0) {
            Object o = map.get(key);
            if (o != null) {
                string = o.toString();
            }
        }
        return string;
    }

    /**
     * @param map
     * @param key
     * @return Integer value.
     */
    public static Integer getInteger(Map<String, Object> map, String key) {
        Integer integer = null;
        if (map != null && map.size() > 0) {
            Object o = map.get(key);
            if (o != null) {
                try {
                    integer = Integer.parseInt(o.toString());
                } catch (NumberFormatException nfe) {
                    integer = null;
                }
            }
        }
        return integer;
    }

    /**
     * @param map
     * @param key
     * @return Array of objects.
     */
    public static Object[] getArray(Map<String, Object> map, String key) {
        Object[] array = null;
        if (map != null && map.size() > 0) {
            Object o = map.get(key);
            array = castToArray(o);
        }
        return array;
    }

    /**
     *
     * @param object
     * @return Array of objects
     */
    public static Object[] castToArray(Object object) {
        Object[] array = null;

        if (object != null) {
            if (object instanceof String) {
                array = EMPTY_ARRAY;
            } else {
                array = (Object[]) object;
            }
        }

        return array;
    }

    /**
     *
     * @param object
     * @return Map of objects
     */
    @SuppressWarnings("unchecked")
    public static Map<String, Object> castToMap(Object object) {
        Map<String, Object> map = null;

        if (object != null) {
            if (object instanceof String) {
                map = EMPTY_MAP;
            } else {
                map = (Map<String, Object>) object;
            }
        }

        return map;
    }

}
