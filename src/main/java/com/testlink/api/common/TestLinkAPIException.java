package com.testlink.api.common;

public class TestLinkAPIException extends Exception {

    /**
     * Error Code.
     */
    private Integer code;

    /**
     * Default constructor.
     */
    public TestLinkAPIException() {
    }

    /**
     * @param message
     */
    public TestLinkAPIException(String message) {
        super(message);
    }

    public TestLinkAPIException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    /**
     * @param cause
     */
    public TestLinkAPIException(Throwable cause) {
        super(cause);
    }

    /**
     * @param message
     * @param cause
     */
    public TestLinkAPIException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @return Error Code
     */
    public Integer getCode() {
        return this.code;
    }
}
