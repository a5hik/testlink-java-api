package com.testlink.api.common;


public class TestLinkConstants {

    public enum Methods {
        //Test Link Methods.
        CREATE_TEST_PROJECT("tl.createTestProject"),
        GET_TEST_PROJECT_BY_NAME("tl.getTestProjectByName"),
        CHECK_DEV_KEY("tl.checkDevKey"),
        CREATE_TEST_SUITE("tl.createTestSuite"),
        SAY_HELLO("tl.sayHello"),
        ABOUT("tl.about");

        private String value;

        Methods(String value) {
            this.value = value;
        }

        public String toString() {
            return this.value;
        }
    }

    public enum Params {
        //Test Link Params
        ID("id"),
        NAME("name"),
        DETAILS("details"),
        DEV_KEY("devKey"),
        NOTES("notes"),
        TEST_PROJECT_NAME("testprojectname"),
        TEST_CASE_PREFIX("testcaseprefix"),
        TEST_PROJECT_ID("testprojectid"),
        TEST_SUITE_ID("testsuiteid"),
        TEST_SUITE_NAME("testsuitename"),
        TITLE("title");

        private String value;

        Params(String value) {
            this.value = value;
        }

        public String toString() {
            return this.value;
        }
    }

    public enum APIErrors {

        //API Errors.
        TEST_PROJECT_NAME_DOES_NOT_EXIST(7011, "");

        private Integer code;
        private String message;

        private APIErrors(int code, String message) {
            this.code = code;
            this.message = message;
        }

        public Integer getCode() {
            return this.code;
        }

        @Override
        public String toString() {
            return this.code.toString() + ": " + this.message;
        }

        public boolean isCode(Integer code) {
            return this.code.equals(code);
        }
    }


}
