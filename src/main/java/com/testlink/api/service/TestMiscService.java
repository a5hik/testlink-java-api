/* 
 * The MIT License
 * 
 * Copyright (c) 2010 Bruno P. Kinoshita <http://www.kinoshita.eti.br>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.testlink.api.service;

import static com.testlink.api.common.TestLinkConstants.Params;
import static com.testlink.api.common.TestLinkConstants.Methods;
import com.testlink.api.common.TestLinkAPIException;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;

import java.util.HashMap;
import java.util.Map;

public class TestMiscService extends BaseService {

    /**
     * @param xmlRpcClient XML RPC Client.
     * @param devKey TestLink User DevKey.
     */
    public TestMiscService(XmlRpcClient xmlRpcClient, String devKey) {
        super(xmlRpcClient, devKey);
    }

    public Boolean checkDevKey(String devKey) throws TestLinkAPIException {
        Boolean statusOk = false;

        try {
            Map<String, Object> executionData = new HashMap<String, Object>();
            executionData.put(Params.DEV_KEY.toString(), devKey);
            Object response = this.executeXmlRpcCall(Methods.CHECK_DEV_KEY.toString(), executionData);
            statusOk = Boolean.valueOf(response.toString());
        } catch (XmlRpcException xmlrpcex) {
            throw new TestLinkAPIException("Error verifying developer key: " + xmlrpcex.getMessage(), xmlrpcex);
        }

        return statusOk;
    }

    /**
     * Says hello.
     * 
     * @return
     * @throws TestLinkAPIException
     */
    public String sayHello() throws TestLinkAPIException {
        String message = null;

        try {
            Object response = this.executeXmlRpcCall(Methods.SAY_HELLO.toString(), null);
            message = (String) response;
        } catch (XmlRpcException xmlrpcex) {
            throw new TestLinkAPIException("Error saying hello: " + xmlrpcex.getMessage(), xmlrpcex);
        }

        return message;
    }

    /**
     * @return
     * @throws TestLinkAPIException
     */
    public String about() throws TestLinkAPIException {
        String message = null;

        try {
            Object response = this.executeXmlRpcCall(Methods.ABOUT.toString(), null);
            message = (String) response;
        } catch (XmlRpcException xmlrpcex) {
            throw new TestLinkAPIException("Error in about method: " + xmlrpcex.getMessage(), xmlrpcex);
        }

        return message;
    }

}
