package com.testlink.api.service;

import static com.testlink.api.common.TestLinkConstants.Methods;
import static com.testlink.api.common.TestLinkConstants.Params;
import com.testlink.api.common.TestLinkAPIException;
import com.testlink.api.common.TestLinkConstants;
import com.testlink.api.common.TestLinkHelper;
import com.testlink.api.domain.TestProject;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;

import java.util.HashMap;
import java.util.Map;


public class TestProjectService extends BaseService {

    /**
     * @param xmlRpcClient XML RPC Client.
     * @param devKey       TestLink User DevKey.
     */
    public TestProjectService(XmlRpcClient xmlRpcClient, String devKey) {
        super(xmlRpcClient, devKey);
    }

    public TestProject getTestProjectByName(String projectName)
            throws TestLinkAPIException {
        TestProject testProject = null;

        try {
            Map<String, Object> executionData = new HashMap<String, Object>();
            executionData.put(Params.TEST_PROJECT_NAME.toString(), projectName);
            Object response = this.executeXmlRpcCall(
                    Methods.GET_TEST_PROJECT_BY_NAME.toString(), executionData);
            Map<String, Object> responseMap = TestLinkHelper.castToMap(response);
            testProject = getTestProject(responseMap);
        } catch (XmlRpcException xmlrpcex) {
            throw new TestLinkAPIException("Error retrieving test project: "
                    + xmlrpcex.getMessage(), xmlrpcex);
        }

        return testProject;
    }

    /**
     * Creates a Test Project.
     *
     * @return Created Test Project object.
     */
    public TestProject createTestProject(String testProjectName,
                                            String testProjectPrefix, String notes)
            throws TestLinkAPIException {
        TestProject testProject = null;

        Integer id = 0;

        testProject = new TestProject(id, testProjectName, notes, testProjectPrefix);

        try {
            Map<String, Object> executionData = getTestProjectMap(testProject);
            Object response = this.executeXmlRpcCall(
                    Methods.CREATE_TEST_PROJECT.toString(), executionData);
            Object[] responseArray = TestLinkHelper.castToArray(response);
            Map<String, Object> responseMap = (Map<String, Object>) responseArray[0];

            id = TestLinkHelper.getInteger(responseMap, Params.ID.toString());
            testProject.setId(id);
        } catch (XmlRpcException xmlrpcex) {
            throw new TestLinkAPIException("Error creating test project: "
                    + xmlrpcex.getMessage(), xmlrpcex);
        }

        return testProject;
    }

    /**
     * Extracts a Test Project from a Map.
     *
     * @param map Map with properties of a Test Project.
     * @return Test Project.
     */
    @SuppressWarnings("unchecked")
    private TestProject getTestProject(Map<String, Object> map) {
        TestProject testProject = null;
        if (map != null && map.size() > 0) {
            Object o = map.get(Params.ID.toString());
            if (o != null) {
                Integer id = Integer.parseInt(o.toString());

                if (id > 0) {
                    testProject = new TestProject();
                    testProject.setId(id);

                    testProject.setName(TestLinkHelper.getString(map, Params.NAME.toString()));
                }
            }
        }
        return testProject;
    }

    /**
     * @param project
     * @return Map of Test Project
     */
    private Map<String, Object> getTestProjectMap(TestProject project) {
        Map<String, Object> executionData = new HashMap<String, Object>();
        executionData.put(Params.TEST_PROJECT_NAME.toString(), project.getName());
        executionData.put(Params.TEST_CASE_PREFIX.toString(), project.getPrefix());
        executionData.put(Params.NOTES.toString(), project.getNotes());
        return executionData;
    }

}
