package com.testlink.api.service;

import static com.testlink.api.common.TestLinkConstants.Methods;
import static com.testlink.api.common.TestLinkConstants.Params;
import com.testlink.api.common.*;
import com.testlink.api.domain.TestSuite;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;

import java.util.HashMap;
import java.util.Map;

public class TestSuiteService extends BaseService {
    /**
     * @param xmlRpcClient XML-RPC Client.
     * @param devKey
     */
    public TestSuiteService(XmlRpcClient xmlRpcClient, String devKey) {
        super(xmlRpcClient, devKey);
    }

    /**
     * Create top level test suite under a specific project identifier
     *
     * @param projectID		Required
     * @param suiteName		Required
     * @param description	Required
     * @return The identifier for the created test suite.
     * @throws TestLinkAPIException
     */
    public TestSuite createTestSuite(
            Integer projectID,
            String suiteName,
            String description) throws TestLinkAPIException
    {
        return createTestSuite(projectID, suiteName, description, null, null, null);
    }

    /**
     *
     * Create a test suite at any level using the project identifier and
     * the parent suite identifier information.
     *
     * @param projectID		Required
     * @param suiteName		Required
     * @param description	Required
     * @param parentId		Optional
     * @param order			Optional
     * @param checkDuplicatedName			Optional
     * @return The identifier for the created test suite.
     * @throws TestLinkAPIException
     */
    public TestSuite createTestSuite(
            Integer projectID,
            String suiteName,
            String description,
            Integer parentId,
            Integer order,
            Boolean checkDuplicatedName) throws TestLinkAPIException
    {
        TestSuite testSuite = null;

        Integer id = 0;

        testSuite = new TestSuite(id, projectID, suiteName, description, parentId, order, checkDuplicatedName);

        try {
            Map<String, Object> executionData = getTestSuiteMap(testSuite);
            Object response = this.executeXmlRpcCall(Methods.CREATE_TEST_SUITE.toString(), executionData);
            Object[] responseArray = TestLinkHelper.castToArray(response);
            Map<String, Object> responseMap = (Map<String, Object>) responseArray[0];

            id = TestLinkHelper.getInteger(responseMap, Params.ID.toString());
            testSuite.setId(id);

            testSuite.setName(suiteName);
        } catch (XmlRpcException xmlrpcex) {
            throw new TestLinkAPIException("Error creating test suite: " + xmlrpcex.getMessage(), xmlrpcex);
        }

        return testSuite;
    }

    /**
     * @param testSuite
     * @return Map of Test Suite Map.
     */
    private Map<String, Object> getTestSuiteMap(TestSuite testSuite) {
        Map<String, Object> executionData = new HashMap<String, Object>();
        executionData.put(Params.TEST_SUITE_NAME.toString(), testSuite.getName());
        executionData.put(Params.TEST_PROJECT_ID.toString(), testSuite.getTestProjectId());
        executionData.put(Params.DETAILS.toString(), testSuite.getDetails());
        return executionData;
    }
}
