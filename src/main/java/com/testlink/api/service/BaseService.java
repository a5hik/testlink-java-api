package com.testlink.api.service;

import static com.testlink.api.common.TestLinkConstants.Params;
import com.testlink.api.common.TestLinkAPIException;
import com.testlink.api.common.TestLinkHelper;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BaseService {

    private static final Integer FALSE_IN_PHP = 0;

    /**
     * XML-RPC client.
     */
    private XmlRpcClient xmlRpcClient;

    /**
     * TestLink User devkey.
     */
    private String devKey;

    /**
     * @param xmlRpcClient
     *            XML-RPC Client.
     * @param devKey
     *            TestLink user DevKey.
     */
    public BaseService(XmlRpcClient xmlRpcClient, String devKey) {
        this.xmlRpcClient = xmlRpcClient;
        this.devKey = devKey;
    }

    /**
     * Executes the XML-RPC call to the method in the server, passing the
     * execution data map.
     *
     * @param methodName
     *            name of the method.
     * @param executionData
     *            execution data map.
     * @return Server response.
     * @throws org.apache.xmlrpc.XmlRpcException
     */
    public Object executeXmlRpcCall(String methodName,
                                    Map<String, Object> executionData) throws XmlRpcException,
            TestLinkAPIException {
        List<Object> params = new ArrayList<Object>();

        if (executionData != null) {
            if (executionData.get(Params.DEV_KEY.toString()) == null) {
                executionData.put(Params.DEV_KEY.toString(),
                        this.devKey);
            }
            params.add(executionData);
        }

        final Object o = this.xmlRpcClient.execute(methodName, params);
        this.checkResponseError(o);
        return o;
    }

    /**
     * @param response
     */
    @SuppressWarnings("unchecked")
    protected void checkResponseError(Object response)
            throws TestLinkAPIException {
        // may be an array of errors (IXError)
        if (response instanceof Object[]) {
            final Object[] responseArray = TestLinkHelper.castToArray(response);
            for (int i = 0; i < responseArray.length; i++) {
                Object maybeAMap = responseArray[i];
                // may be a map with error code and message
                if (maybeAMap instanceof Map<?, ?>) {
                    Map<String, Object> errorMap = (Map<String, Object>) maybeAMap;
                    Integer code = TestLinkHelper.getInteger(errorMap, "code");
                    String message = TestLinkHelper.getString(errorMap, "message");

                    if (code != null) {
                        throw new TestLinkAPIException(code, message);
                    }

                } // endif
            } // endfor
        } else if (response instanceof Map<?, ?>) {
            final Map<String, Object> errorMap = (Map<String, Object>) response;
            final Integer statusOk = TestLinkHelper.getInteger(errorMap, "status_ok");
            final String message = TestLinkHelper.getString(errorMap, "msg");
            if (statusOk != null && statusOk.equals(FALSE_IN_PHP)) {
                throw new TestLinkAPIException(statusOk, message);
            }
        }
    }

}
