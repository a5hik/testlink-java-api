package com.testlink.api.domain;

import java.io.Serializable;

public class TestProject implements Serializable {

    private Integer id;

    private String name;

    private String notes;

    private String prefix;

    public TestProject() {

    }

    public TestProject(Integer id, String name, String notes, String prefix) {

        this.id = id;
        this.name = name;
        this.notes = notes;
        this.prefix = prefix;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

}
