package com.testlink.api.domain;

import java.io.Serializable;

public class TestSuite implements Serializable {

    private Integer id;
    private Integer testProjectId;
    private String name;
    private String details;
    private Integer parentId;
    private Integer order;
    private Boolean checkDuplicatedName;

    public TestSuite(Integer id, Integer testProjectId, String name, String details,
                     Integer parentId, Integer order, Boolean checkDuplicatedName) {
        this.id = id;
        this.testProjectId = testProjectId;
        this.name = name;
        this.details = details;
        this.parentId = parentId;
        this.order = order;
        this.checkDuplicatedName = checkDuplicatedName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTestProjectId() {
        return testProjectId;
    }

    public void setTestProjectId(Integer testProjectId) {
        this.testProjectId = testProjectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Boolean getCheckDuplicatedName() {
        return checkDuplicatedName;
    }

    public void setCheckDuplicatedName(Boolean checkDuplicatedName) {
        this.checkDuplicatedName = checkDuplicatedName;
    }

}
