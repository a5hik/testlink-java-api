package com.testlink.api;

import com.testlink.api.common.TestLinkAPIException;
import com.testlink.api.domain.TestProject;
import com.testlink.api.domain.TestSuite;
import com.testlink.api.service.TestMiscService;
import com.testlink.api.service.TestProjectService;
import com.testlink.api.service.TestSuiteService;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

import java.net.MalformedURLException;
import java.net.URL;

public class TestLinkAPIClient {

    /* API Initialization variables */
    private String devKey;

    /**
     * The TestLink API URL. See the TestLink API documentation for more information.
     * <p>
     * Example: http://demo.testlink.org/latest/lib/api/xmlrpc/v1/xmlrpc.php
     */
    private String apiUrl;

    private final TestSuiteService testSuiteService;

    private final TestProjectService testProjectService;

    private final TestMiscService testMiscService;

        /**
         * XML-RPC client.
     */
    private XmlRpcClient xmlRpcClient;

    public TestLinkAPIClient(String apiUrl, String devKey) throws TestLinkAPIException {
        this.apiUrl = apiUrl;
        this.devKey = devKey;

        this.xmlRpcClient = new XmlRpcClient();

        // configuration
        final XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
        try {
            config.setServerURL(new URL(apiUrl));
        } catch (MalformedURLException e) {
            throw new TestLinkAPIException("Invalid server Url: " + e.getMessage());
        }
        this.xmlRpcClient.setConfig(config);

        this.testSuiteService = new TestSuiteService(xmlRpcClient, devKey);
        this.testProjectService = new TestProjectService(xmlRpcClient, devKey);
        this.testMiscService = new TestMiscService(xmlRpcClient, devKey);
    }

    /**
     * @return XML-RPC Client.
     */
    public XmlRpcClient getXmlRpcClient() {
        return this.xmlRpcClient;
    }

    /**
     * Retrieves a Test Project by its name.
     *
     * @param projectName Test Project name.
     * @return Test Project with given name or null if not found.
     * @throws TestLinkAPIException
     */
    public TestProject getTestProjectByName(String projectName) throws TestLinkAPIException {
        return this.testProjectService.getTestProjectByName(projectName);
    }

        /* XX Test Suite operations XX */

    public TestSuite createTestSuite(Integer testProjectId, String name, String details)
            throws TestLinkAPIException {
        return this.testSuiteService.createTestSuite(testProjectId, name, details);
    }

    public String getAboutTestLink() throws TestLinkAPIException {
        return this.testMiscService.about();
    }

    public static void main(String args[]) {

        try {
            TestLinkAPIClient client = new TestLinkAPIClient("http://demo.testlink.org/latest/lib/api/xmlrpc/v1/xmlrpc.php", "4f4c8d5e2bc5b573556b89d0db9b34e6");
            TestProject project = client.getTestProjectByName("ctftest");
            client.createTestSuite(project.getId(), "artf102345", "created from API");
        } catch (TestLinkAPIException e) {
            e.printStackTrace();
        }
        //System.out.println(client.testMiscService.about());
    }


}
